package com.smokiyenko.zonetest;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.smokiyenko.zonetest.databinding.ActivityMainBinding;
import com.smokiyenko.zonetest.rippleanimation.CustomRippleAnimator;


public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //TODO improve with retrolyambda
        binding.startAnimationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("aaaaa", "onClick: ");
                new CustomRippleAnimator(binding.activityMain, Color.GREEN)
                        .setAmountOfCircles(5)
                        .setDuration(1000)
                        .start();

            }
        });

    }

}
