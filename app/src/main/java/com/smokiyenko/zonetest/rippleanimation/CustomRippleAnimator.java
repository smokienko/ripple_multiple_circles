package com.smokiyenko.zonetest.rippleanimation;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;

/**
 * Created by smokiyenko on 10/2/16.
 */

public class CustomRippleAnimator {

    private static final int DEFAULT_DURATION = 1000;
    private static final int DEFAULT_CIRCLES_AMOUNT = 3;
    private static final int DEFAULT_START_COLOR = Color.WHITE;

    private View view;

    private int endColor;
    private int startColor;
    private float maxRadius;
    private long animationDuration;
    private int amountOfCircles;
    private Paint.Style paintStyle;

    private CustomRippleDrawable rippleDrawable;

    public CustomRippleAnimator(View view, int endColor) {
        this.view = view;
        this.endColor = endColor;
        setMaxRadius(0f);
        setStartColor(DEFAULT_START_COLOR);
        setDuration(DEFAULT_DURATION);
        setAmountOfCircles(DEFAULT_CIRCLES_AMOUNT);
        setPaintStyle(Paint.Style.FILL);
    }

    private float initMaxRadius(float maxRadius, View view) {
        if (maxRadius < 1f) {
            return view.getWidth() > view.getHeight() ? view.getWidth() : view.getHeight();
        }
        return maxRadius;
    }

    private void initAnimatedDrawable() {
        Drawable background = view.getBackground();
        int leftPadding = view.getPaddingLeft();
        int rightPadding = view.getPaddingRight();
        int topPadding = view.getPaddingTop();
        int bottomPadding = view.getPaddingBottom();
        if (background != null) {
            if (background instanceof LayerDrawable) {
                LayerDrawable layerDrawable = (LayerDrawable) background;
                if (layerDrawable.getDrawable(1) instanceof CustomRippleDrawable) {
                    background = layerDrawable.getDrawable(0);
                }
            }
            Drawable[] layers = new Drawable[2];
            layers[0] = background;
            layers[1] = rippleDrawable;
            LayerDrawable layerDrawable = new LayerDrawable(layers);
            view.setBackground(layerDrawable);
        } else {
            background = rippleDrawable;
            view.setBackground(background);
        }
        view.setPadding(leftPadding,topPadding,rightPadding,bottomPadding);
    }



    public CustomRippleAnimator setStartColor(int startColor) {
        this.startColor = startColor;
        return this;
    }

    public CustomRippleAnimator setAmountOfCircles(int amountOfCircles) {
        this.amountOfCircles = amountOfCircles;
        return this;
    }

    public CustomRippleAnimator setDuration(long duration) {
        this.animationDuration = duration;
        return this;
    }

    public void setMaxRadius(float maxRadius) {
        this.maxRadius = initMaxRadius(maxRadius,view);
    }

    public void setPaintStyle(Paint.Style paintStyle) {
        this.paintStyle = paintStyle;
    }

    public void start() {
        if (rippleDrawable == null) {
            rippleDrawable = new CustomRippleDrawable.Builder(endColor,maxRadius)
                    .setAmountOfCircles(amountOfCircles)
                    .setAnimationDuration(animationDuration)
                    .setStartColor(startColor)
                    .setPaintStyle(paintStyle)
                    .build();
            initAnimatedDrawable();
        }
        view.post(rippleDrawable);
    }
}
