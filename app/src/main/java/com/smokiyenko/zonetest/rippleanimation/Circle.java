package com.smokiyenko.zonetest.rippleanimation;

import android.graphics.Paint;

/**
 * Created by smokiyenko on 9/30/16.
 */

class Circle {

    private Paint paint;

    private float radius;

    Circle(Paint paint, float radius) {
        this.paint = paint;
        this.radius = radius;
    }

    Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }

    float getRadius() {
        return radius;
    }

    void setRadius(float radius) {
        this.radius = radius;
    }

    Circle duplicate(){
        return new Circle(paint, radius);
    }
}
