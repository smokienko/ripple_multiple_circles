package com.smokiyenko.zonetest.rippleanimation;

import android.animation.ArgbEvaluator;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by smokiyenko on 9/29/16.
 */

class CustomRippleDrawable extends Drawable implements Drawable.Callback, Runnable {

    private static final int DRAWING_INTERVAL = 15;

    private List<Paint> paints;
    private List<Circle> drawingCircles;
    private double intermediateRadius;

    private int startColor;
    private int endColor;
    private long animLength;
    private int amountOfCircles;
    private float maxRadius;
    private float sizeIncrement;
    private Paint.Style paintStyle;

    private CustomRippleDrawable(int startColor,
                                 int endColor,
                                 long animLength,
                                 int amountOfCircles,
                                 float maxRadius,
                                 Paint.Style paintStyle) {

        this.startColor = startColor;
        this.endColor = endColor;
        this.animLength = animLength;
        this.amountOfCircles = amountOfCircles;
        this.maxRadius = maxRadius;
        this.paintStyle = paintStyle;

        init();
    }

    static class Builder {

        private int startColor;
        private int endColor;
        private long animLength;
        private int amountOfCircles;
        private float maxRadius;
        private Paint.Style paintStyle;

        Builder(int endColor, float maxRadius) {
            this.endColor = endColor;
            this.maxRadius = maxRadius;
        }

        Builder setStartColor(int startColor) {
            this.startColor = startColor;
            return this;
        }

        Builder setAnimationDuration(long animLength) {
            this.animLength = animLength;
            return this;
        }

        Builder setAmountOfCircles(int amountOfCircles) {
            this.amountOfCircles = amountOfCircles;
            return this;
        }

        Builder setPaintStyle(Paint.Style paintStyle) {
            this.paintStyle = paintStyle;
            return this;
        }

        CustomRippleDrawable build() {
            return new CustomRippleDrawable(startColor, endColor, animLength, amountOfCircles, maxRadius, paintStyle);
        }
    }

    private void init() {
        ArgbEvaluator evaluator = new ArgbEvaluator();
        paints = new ArrayList<>();
        drawingCircles = new ArrayList<>();
        intermediateRadius = maxRadius / amountOfCircles;
        sizeIncrement = (maxRadius * 2) / (animLength / DRAWING_INTERVAL);
        float step = 1.0f / amountOfCircles;
        for (float i = 0f; i < 1.000001f; i = i + step) {
            Paint paint = new Paint();
            paint.setColor((Integer) evaluator.evaluate(i, endColor, startColor));
            paint.setAntiAlias(true);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(paintStyle);
            paint.setStrokeWidth(5);
            paints.add(paint);
        }
        drawingCircles.add(new Circle(paints.get(0), 0));
    }

    @Override
    public void invalidateDrawable(@NonNull Drawable drawable) {
        super.invalidateSelf();
    }

    @Override
    public void scheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable, long l) {
        invalidateDrawable(drawable);
    }

    @Override
    public void unscheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable) {
        super.unscheduleSelf(runnable);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        canvas.save();
        Rect bounds = getBounds();
        for (Circle circle : drawingCircles) {
            canvas.drawCircle(bounds.centerX(), bounds.centerY(), circle.getRadius(), circle.getPaint());
        }
    }

    private void nextFrame() {
        unscheduleSelf(this);
        if (drawingCircles.get(drawingCircles.size() - 1).getRadius() < maxRadius) {
            scheduleSelf(this, SystemClock.uptimeMillis() + DRAWING_INTERVAL);
        } else {
            drawingCircles.clear();
            drawingCircles.add(new Circle(paints.get(0), 0));
        }
    }

    public void run() {
        for (Circle circle : drawingCircles) {
            if (circle.getRadius() < maxRadius) {
                circle.setRadius(circle.getRadius() + sizeIncrement);
            }
        }
        if (drawingCircles.size() != paints.size()
                && drawingCircles.get(drawingCircles.size() - 1).getRadius() > intermediateRadius) {
            drawingCircles.add(new Circle(paints.get(drawingCircles.size()), 0));
        }
        invalidateSelf();
        nextFrame();
    }

    @Override
    public void setAlpha(int alpha) {
        //Not needed
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        //Not needed
    }

    @Override
    public int getOpacity() {
        // Not Implemented
        return PixelFormat.UNKNOWN;
    }
}